import { Cliente } from "./cliente.interface";

export interface Pago {
    monthExp: string,
    yearExp: string,
    ccv: number,
    nomTitular: number,
    cliente: Cliente
}