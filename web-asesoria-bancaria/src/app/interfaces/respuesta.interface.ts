import { Consulta } from "./consulta.interface";

export interface Respuesta {
    desRespuesta : string,
    fechaRespuesta : string
    consulta: Consulta
}