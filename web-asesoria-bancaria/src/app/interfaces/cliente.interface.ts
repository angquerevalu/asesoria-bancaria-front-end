export interface Cliente {
    nomCliente: string;
    apeCliente: string;
    dniCliente: string;
    fonoCliente: number;
    emailCliente: string;
    passCliente: string;
}

export interface ClienteResponse {
    codCliente: number,
    nomCliente: string;
    apeCliente: string;
    dniCliente: string;
    fonoCliente: number;
    emailCliente: string;
    passCliente: string;
}