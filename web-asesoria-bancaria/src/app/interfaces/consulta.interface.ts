import { Cliente } from "./cliente.interface";

export interface Consulta {
    codConsulta: number,
    desConsulta: string,
    fechaConsulta: string,
    estado: Estado,
    cliente: Cliente,
    categoria: Categoria
}

export interface Estado{
    codEstado: number,
    desEstado: string
}

export interface Categoria{
    codCategoria: number,
    desCategoria: string,
    precioCategoria: string,
}