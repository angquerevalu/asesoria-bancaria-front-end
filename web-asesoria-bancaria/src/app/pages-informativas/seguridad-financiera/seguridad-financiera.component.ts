import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RutasConstante } from 'src/app/libs/rutas-constante';

@Component({
  selector: 'app-seguridad-financiera',
  templateUrl: './seguridad-financiera.component.html',
  styleUrls: ['./seguridad-financiera.component.css']
})
export class SeguridadFinancieraComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  public ingresar(){
    this.router.navigate([RutasConstante.INGRESAR_SISTEMA]);
  }

  public inicio(){
    this.router.navigate([RutasConstante.PAGES_INFORMATIVAS]);
  }

  public cargosComunes(){
    this.router.navigate([RutasConstante.CARGOS_COMUNES]);
  }

  public cuentaBancaria(){
    this.router.navigate([RutasConstante.CUENTA_BANCARIA]);
  }

  public operacionesActivasPasivas(){
    this.router.navigate([RutasConstante.OPERACIONES_ACTIVAS_PASIVAS]);
  }

}
