import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SeguridadFinancieraComponent } from './seguridad-financiera.component';

describe('SeguridadFinancieraComponent', () => {
  let component: SeguridadFinancieraComponent;
  let fixture: ComponentFixture<SeguridadFinancieraComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SeguridadFinancieraComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SeguridadFinancieraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
