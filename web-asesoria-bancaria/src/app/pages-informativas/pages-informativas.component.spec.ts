import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PagesInformativasComponent } from './pages-informativas.component';

describe('PagesInformativasComponent', () => {
  let component: PagesInformativasComponent;
  let fixture: ComponentFixture<PagesInformativasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PagesInformativasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PagesInformativasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
