import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RutasConstante } from 'src/app/libs/rutas-constante';

@Component({
  selector: 'app-presupuesto',
  templateUrl: './presupuesto.component.html',
  styleUrls: ['./presupuesto.component.css']
})
export class PresupuestoComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  public ingresar(){
    this.router.navigate([RutasConstante.INGRESAR_SISTEMA]);
  }

  public inicio(){
    this.router.navigate([RutasConstante.PAGES_INFORMATIVAS]);
  }

}
