import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RutasConstante } from '../libs/rutas-constante';

@Component({
  selector: 'app-pages-informativas',
  templateUrl: './pages-informativas.component.html',
  styleUrls: ['./pages-informativas.component.css']
})
export class PagesInformativasComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  public ingresar(){
    this.router.navigate([RutasConstante.INGRESAR_SISTEMA]);
  }

  public tarjetaCredito(){
    this.router.navigate([RutasConstante.USO_TARJETAS]);
  }

  public presupuesto(){
    this.router.navigate([RutasConstante.PRESUPUESTO]);
  }

  public seguridadFinanciera(){
    this.router.navigate([RutasConstante.SEGURIDAD_FINANCIERA]);
  }

}
