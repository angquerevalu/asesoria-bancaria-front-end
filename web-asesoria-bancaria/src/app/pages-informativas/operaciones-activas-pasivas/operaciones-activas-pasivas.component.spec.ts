import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OperacionesActivasPasivasComponent } from './operaciones-activas-pasivas.component';

describe('OperacionesActivasPasivasComponent', () => {
  let component: OperacionesActivasPasivasComponent;
  let fixture: ComponentFixture<OperacionesActivasPasivasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OperacionesActivasPasivasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OperacionesActivasPasivasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
