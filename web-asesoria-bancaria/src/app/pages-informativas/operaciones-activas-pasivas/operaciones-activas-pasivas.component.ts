import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RutasConstante } from 'src/app/libs/rutas-constante';

@Component({
  selector: 'app-operaciones-activas-pasivas',
  templateUrl: './operaciones-activas-pasivas.component.html',
  styleUrls: ['./operaciones-activas-pasivas.component.css']
})
export class OperacionesActivasPasivasComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  public inicio(){
    this.router.navigate([RutasConstante.PAGES_INFORMATIVAS]);
  }

  public ingresar(){
    this.router.navigate([RutasConstante.INGRESAR_SISTEMA]);
  }

}
