import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RutasConstante } from 'src/app/libs/rutas-constante';

@Component({
  selector: 'app-cargos-comunes-frecuentes',
  templateUrl: './cargos-comunes-frecuentes.component.html',
  styleUrls: ['./cargos-comunes-frecuentes.component.css']
})
export class CargosComunesFrecuentesComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  public inicio(){
    this.router.navigate([RutasConstante.PAGES_INFORMATIVAS]);
  }

  public ingresar(){
    this.router.navigate([RutasConstante.INGRESAR_SISTEMA]);
  }

}
