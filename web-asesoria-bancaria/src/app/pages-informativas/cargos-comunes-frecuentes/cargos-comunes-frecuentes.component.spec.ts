import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CargosComunesFrecuentesComponent } from './cargos-comunes-frecuentes.component';

describe('CargosComunesFrecuentesComponent', () => {
  let component: CargosComunesFrecuentesComponent;
  let fixture: ComponentFixture<CargosComunesFrecuentesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CargosComunesFrecuentesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CargosComunesFrecuentesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
