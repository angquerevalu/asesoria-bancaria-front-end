import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RutasConstante } from 'src/app/libs/rutas-constante';

@Component({
  selector: 'app-tarjeta-credito',
  templateUrl: './tarjeta-credito.component.html',
  styleUrls: ['./tarjeta-credito.component.css']
})
export class TarjetaCreditoComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  public ingresar(){
    this.router.navigate([RutasConstante.INGRESAR_SISTEMA]);
  }

  public inicio(){
    this.router.navigate([RutasConstante.PAGES_INFORMATIVAS]);
  }

}
