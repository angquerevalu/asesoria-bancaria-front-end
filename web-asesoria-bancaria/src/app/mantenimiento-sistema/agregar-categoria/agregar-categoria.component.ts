import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Categoria } from 'src/app/interfaces/consulta.interface';
import { RegistroCategoriaService } from 'src/app/services/registro-categoria.service';

@Component({
  selector: 'app-agregar-categoria',
  templateUrl: './agregar-categoria.component.html',
  styleUrls: ['./agregar-categoria.component.css']
})
export class AgregarCategoriaComponent implements OnInit {

  public form: FormGroup;
  @Input() registro: Categoria;
  @Input() inputIndex: number;
  @Input() lstCategorias: Categoria[];
  @Output() lstRegistroActualizado = new EventEmitter<any>()
  @Output() exito = new EventEmitter<any>();

  constructor(private fb: FormBuilder,
    private activeModal: NgbActiveModal,
    private categoriaService: RegistroCategoriaService) { this.form = this.createForm(); }

  ngOnInit(): void {
    if (this.registro) {
      this.setValuesEdit(this.registro);
    } else {
      this.categoriaService.ultimoCodigo().subscribe((data) => {
        if (data == 0) {
          this.fieldCodCategoria.setValue(1);
        } else {
          this.fieldCodCategoria.setValue(data + 1);
        }
      })
    }
  }

  private createForm() {
    return this.fb.group({
      codCategoria: [null, [Validators.required]],
      desCategoria: [null, [Validators.required, Validators.pattern]],
      preCategoria: [null, [Validators.required, Validators.pattern]]
    })
  }

  public get getForm() {
    return this.form;
  }

  public get fieldCodCategoria() {
    return this.getForm.get('codCategoria');
  }

  public get fieldDesCategoria() {
    return this.getForm.get('desCategoria');
  }

  public get fieldPreCategoria() {
    return this.getForm.get('preCategoria');
  }

  public cerrar() {
    this.activeModal.close();
  }

  public agregar() {
    //Validaciones
    this.getForm.markAllAsTouched();
    if (this.getForm.invalid) return;

    const registro: Categoria = {
      codCategoria: this.fieldCodCategoria.value,
      desCategoria: this.fieldDesCategoria.value,
      precioCategoria: this.fieldPreCategoria.value
    }

    if (!this.registro) {
      this.lstRegistroActualizado.emit(registro);
      this.categoriaService.registrarCategoria(registro).subscribe();
    } else {
      const registroActualizado: Categoria = {
        codCategoria: this.registro.codCategoria,
        desCategoria: this.fieldDesCategoria.value,
        precioCategoria: this.fieldPreCategoria.value
      }
      if (!this.equals(this.registro, registroActualizado)) {
        this.lstCategorias[this.inputIndex] = registroActualizado;
        this.categoriaService.modificarCategoria(registroActualizado).subscribe();
      }
    }


    this.activeModal.close();
  }

  private setValuesEdit(categoria: Categoria): void {
    this.fieldCodCategoria.setValue(categoria.codCategoria);
    this.fieldDesCategoria.setValue(categoria.desCategoria);
    this.fieldPreCategoria.setValue(categoria.precioCategoria);
  }

  private equals(obj: Categoria, objNuevo: Categoria): boolean {
    return (objNuevo.codCategoria === obj.codCategoria &&
      objNuevo.desCategoria === obj.desCategoria &&
      objNuevo.precioCategoria === obj.precioCategoria)
  }

}
