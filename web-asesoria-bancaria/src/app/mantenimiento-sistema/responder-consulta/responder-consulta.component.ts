import { DatePipe } from '@angular/common';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Consulta, Estado } from 'src/app/interfaces/consulta.interface';
import { Respuesta } from 'src/app/interfaces/respuesta.interface';
import { ParametriaService } from 'src/app/services/parametria.service';
import { RegistroConsultaService } from 'src/app/services/registro-consulta.service';
import { RespuestaService } from 'src/app/services/respuesta.service';

@Component({
  selector: 'app-responder-consulta',
  templateUrl: './responder-consulta.component.html',
  styleUrls: ['./responder-consulta.component.css']
})
export class ResponderConsultaComponent implements OnInit {

  public form: FormGroup;
  public lstEstado = [];
  @Input() consulta: Consulta;
  @Output() exito = new EventEmitter<any>();

  constructor(private fb: FormBuilder,
    private activeModal: NgbActiveModal,
    public datePipe: DatePipe,
    private parametriaService: ParametriaService,
    private respuestaService: RespuestaService,
    private consultaService: RegistroConsultaService) {
      this.form = this.createForm();
     }

     private createForm() {
      return this.fb.group({
        desConsulta: [null, [Validators.required]],
        desRespuesta: [null, [Validators.required]],
        estado: [null, []],
        fechaRespuesta: [null, [Validators.required]]
      })
    }
  
    public get getForm() {
      return this.form;
    }
  
    public get fieldDesConsulta() {
      return this.getForm.get('desConsulta');
    }

    public get fieldDesRespuesta() {
      return this.getForm.get('desRespuesta');
    }
  
    public get fieldFechaRespuesta() {
      return this.getForm.get('fechaRespuesta');
    }
  
    public get fieldEstado() {
      return this.getForm.get('estado');
    }

  ngOnInit(): void {
    this.fechaConsulta();
    this.parametriaService.getLstEstado().subscribe((data) => {
      this.fieldEstado.setValue(this.consulta.estado.codEstado);
      this.lstEstado = data;
    })
    this.fieldDesConsulta.setValue(this.consulta.desConsulta);
  }

  public fechaConsulta() {
    const fechaActual = String(new Date());
    const fechaRespuesta = this.datePipe.transform(fechaActual, "yyyy-MM-dd");

    this.fieldFechaRespuesta.setValue(fechaRespuesta);
  }

  public cerrar() {
    this.activeModal.close();
  }

  public responder(){
    const updateEstado : Estado = {
      codEstado: this.fieldEstado.value,
      desEstado: this.obtenerDescripcionEstado(this.fieldEstado.value)
    }

    const updateConsulta : Consulta = {
      codConsulta: this.consulta.codConsulta,
      desConsulta: this.consulta.desConsulta,
    fechaConsulta: this.consulta.fechaConsulta,
    estado: updateEstado,
    cliente: this.consulta.cliente,
    categoria: this.consulta.categoria
    }

    const respuesta : Respuesta = {
      desRespuesta: this.fieldDesRespuesta.value,
      fechaRespuesta: this.fieldFechaRespuesta.value,
      consulta: updateConsulta
    }

    this.consultaService.registrarConsulta(updateConsulta).subscribe();
    this.respuestaService.registrarRespuesta(respuesta).subscribe((data) => this.exito.emit(data));
    this.activeModal.close();
  }

  public obtenerDescripcionEstado(val: string):string{
    const desc = this.lstEstado.filter((x) => x.codEstado ===val);
    return desc.length !== 0 ? desc[0].desEstado : "";
  }

}
