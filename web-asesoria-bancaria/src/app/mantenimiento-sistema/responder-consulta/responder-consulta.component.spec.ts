import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResponderConsultaComponent } from './responder-consulta.component';

describe('ResponderConsultaComponent', () => {
  let component: ResponderConsultaComponent;
  let fixture: ComponentFixture<ResponderConsultaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResponderConsultaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResponderConsultaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
