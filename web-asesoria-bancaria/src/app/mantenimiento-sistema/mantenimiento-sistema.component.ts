import { DatePipe } from '@angular/common';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { Categoria, Consulta, Estado } from '../interfaces/consulta.interface';
import { dtOptions } from '../libs/config';
import { Mensajes } from '../libs/mensajes.constantes';
import { RenderizarPaginacion } from '../libs/renderizar-paginacion';
import { RutasConstante } from '../libs/rutas-constante';
import { ModalService } from '../services/modal.service';
import { ParametriaService } from '../services/parametria.service';
import { RegistroCategoriaService } from '../services/registro-categoria.service';
import { RegistroEstadoService } from '../services/registro-estado.service';
import { AgregarCategoriaComponent } from './agregar-categoria/agregar-categoria.component';
import { AgregarEstadoComponent } from './agregar-estado/agregar-estado.component';
import { ResponderConsultaComponent } from './responder-consulta/responder-consulta.component';

@Component({
  selector: 'app-mantenimiento-sistema',
  templateUrl: './mantenimiento-sistema.component.html',
  styleUrls: ['./mantenimiento-sistema.component.css']
})
export class MantenimientoSistemaComponent extends RenderizarPaginacion implements OnInit {
  tab: any = "tab0";
  public isEstado = false;
  public isCategoria = false;
  public lstConsultas = [];
  public atenderConsultas = false;

  @Input() lstEstados = [];
  @Input() lstCategorias = [];

  public dtOptions: DataTables.Settings = dtOptions;
  public dtTrigger: Subject<any> = new Subject();

  public json :any;

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

  constructor(private modalService: NgbModal,
    private parametriaService: ParametriaService,
    public datePipe: DatePipe,
    private modalModule: ModalService,
    private router: Router,
    private estadoService: RegistroEstadoService,
    private categoriaService: RegistroCategoriaService) { super(); }

  ngOnInit(): void {
    this.json = JSON.parse(localStorage.getItem("cliente"));
    if(this.json == null){
      this.router.navigate([RutasConstante.INGRESAR_SISTEMA]);
    }else{
    this.actualizarData();
    this.rerender();
    }
  }

  ngAfterViewInit(): void { this.dtTrigger.next(); }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }

  public actualizarData() {
    this.parametriaService.getConsultas().subscribe((data) => {
      this.lstConsultas = data;
    });
    this.parametriaService.getLstEstado().subscribe((data) => {
      this.lstEstados = data;
    });
    this.parametriaService.getLstCategoria().subscribe((data) => {
      this.lstCategorias = data;
    });
  }

  public activar(check) {
    if (check == 0) {
      this.tab = 'tab1';
      this.isEstado = true;
      this.isCategoria = false;
      this.ngOnInit();
    } else if (check == 1) {
      this.tab = 'tab2';
      this.isCategoria = true;
      this.isEstado = false;
      this.ngOnInit();
    }
  }

  public agregarEstado() {
    const modal = this.modalService.open(AgregarEstadoComponent, {
      backdrop: "static",
      keyboard: false,
      size: 'lg'
    });

    modal.componentInstance.lstRegistroActualizado.subscribe((data) => {
      this.lstEstados.push(data);
      this.rerender();
    })
  }

  public agregarCategoria() {
    const modal = this.modalService.open(AgregarCategoriaComponent, {
      backdrop: "static",
      keyboard: false,
      size: 'lg'
    });

    modal.componentInstance.lstRegistroActualizado.subscribe((data) => {
      this.lstCategorias.push(data);
      this.rerender();
    })
  }

  public fechaConsulta(fecha: string) {
    const fechaActual = String(fecha);
    const fechaConsulta = this.datePipe.transform(fechaActual, "yyyy-MM-dd");

    return fechaConsulta;
  }

  public responder(consulta: Consulta) {
    const modal = this.modalService.open(ResponderConsultaComponent, {
      backdrop: "static",
      keyboard: false,
      size: 'lg'
    });
    modal.componentInstance.consulta = consulta;

    modal.componentInstance.exito.subscribe((data) => {
      if (data != null) {
        this.ngOnInit();
      }
    })
  }

  public editarCategoria(item: Categoria, index: number) {
    const modal = this.modalService.open(AgregarCategoriaComponent, {
      backdrop: "static",
      keyboard: false,
      size: 'lg'
    });

    modal.componentInstance.registro = item;
    modal.componentInstance.inputIndex = index;
    modal.componentInstance.lstCategorias = this.lstCategorias;
  }

  public editarEstado(item: Estado, index: number) {
    const modal = this.modalService.open(AgregarEstadoComponent, {
      backdrop: "static",
      keyboard: false,
      size: 'lg'
    });

    modal.componentInstance.registro = item;
    modal.componentInstance.inputIndex = index;
    modal.componentInstance.lstEstados = this.lstEstados;
  }

  public eliminarCategoria(cod: number, i: number) {
    this.modalModule.showConfirmar("Mensaje", Mensajes.ELIMINAR, 'NO', 'SI').subscribe((resp) => {
      if (resp) {
        this.categoriaService.eliminarCategoria(cod).subscribe();
        this.lstCategorias.splice(i, 1);
      }
    })
  }

  public eliminarEstado(cod: number, i: number) {
    this.modalModule.showConfirmar("Mensaje", Mensajes.ELIMINAR, 'NO', 'SI').subscribe((resp) => {
      if (resp) {
        console.log(cod);
        this.estadoService.eliminarEstado(cod).subscribe();
        this.lstEstados.splice(i, 1);
      }
    })
  }

  public changue(event) {
    this.atenderConsultas = event.currentTarget.checked;
    if (this.atenderConsultas) {
      this.actualizarData();
    }
  }

  public salir(){
    this.router.navigate([RutasConstante.INGRESAR_SISTEMA]);
    localStorage.setItem("cliente", null);
  }

}
