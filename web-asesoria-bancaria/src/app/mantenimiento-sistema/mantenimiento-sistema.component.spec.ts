import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MantenimientoSistemaComponent } from './mantenimiento-sistema.component';

describe('MantenimientoSistemaComponent', () => {
  let component: MantenimientoSistemaComponent;
  let fixture: ComponentFixture<MantenimientoSistemaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MantenimientoSistemaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MantenimientoSistemaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
