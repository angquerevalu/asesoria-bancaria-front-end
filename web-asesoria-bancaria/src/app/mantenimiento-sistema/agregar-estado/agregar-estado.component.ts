import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Estado } from 'src/app/interfaces/consulta.interface';
import { RegistroEstadoService } from 'src/app/services/registro-estado.service';

@Component({
  selector: 'app-agregar-estado',
  templateUrl: './agregar-estado.component.html',
  styleUrls: ['./agregar-estado.component.css']
})
export class AgregarEstadoComponent implements OnInit {

  public form: FormGroup;
  @Input() registro: Estado;
  @Input() inputIndex: number;
  @Input() lstEstados: Estado[];
  @Output() lstRegistroActualizado = new EventEmitter<any>();

  constructor(private fb: FormBuilder,
    private activeModal: NgbActiveModal,
    private estadoService: RegistroEstadoService) {
    this.form = this.createForm();
  }

  ngOnInit(): void {
    if (this.registro) {
      this.setValuesEdit(this.registro);
    } else {
      this.estadoService.ultimoCodigo().subscribe((data) => {
        if (data == 0) {
          this.fieldCodEstado.setValue(1);
        } else {
          this.fieldCodEstado.setValue(data + 1);
        }
      })
    }
  }

  private createForm() {
    return this.fb.group({
      codEstado: [null, [Validators.required]],
      desEstado: [null, [Validators.required, Validators.pattern]]
    })
  }

  public get getForm() {
    return this.form;
  }

  public get fieldCodEstado() {
    return this.getForm.get('codEstado');
  }

  public get fieldDesEstado() {
    return this.getForm.get('desEstado');
  }

  public cerrar() {
    this.activeModal.close();
  }

  public agregar() {
    //Validaciones
    this.getForm.markAllAsTouched();
    if (this.getForm.invalid) return;

    const registro: Estado = {
      codEstado: Number(this.fieldCodEstado.value),
      desEstado: this.fieldDesEstado.value
    }

    if (!this.registro) {
      this.lstRegistroActualizado.emit(registro);
      this.estadoService.registrarEstado(registro).subscribe();
    } else {
      const registroActualizado: Estado = {
        codEstado: this.registro.codEstado,
        desEstado: this.fieldDesEstado.value
      }
      if (!this.equals(this.registro, registroActualizado)) {
        this.lstEstados[this.inputIndex] = registroActualizado;
        this.estadoService.modificarEstado(registroActualizado).subscribe();
      }
    }


    this.activeModal.close();
  }

  private setValuesEdit(estado: Estado): void {
    this.fieldCodEstado.setValue(estado.codEstado);
    this.fieldDesEstado.setValue(estado.desEstado);
  }

  private equals(obj: Estado, objNuevo: Estado): boolean {
    return (objNuevo.codEstado === obj.codEstado &&
      objNuevo.desEstado === obj.desEstado)
  }

}
