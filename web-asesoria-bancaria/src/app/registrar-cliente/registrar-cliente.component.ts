import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { Cliente } from '../interfaces/cliente.interface';
import { RutasConstante } from '../libs/rutas-constante';
import { RegistroClienteService } from '../services/registro-cliente.service';

@Component({
  selector: 'app-registrar-cliente',
  templateUrl: './registrar-cliente.component.html',
  styleUrls: ['./registrar-cliente.component.css']
})
export class RegistrarClienteComponent implements OnInit {

  public bool = false;
  public formCliente: FormGroup;
  public msg = "Campo requerido";

  constructor(private fb: FormBuilder, private router: Router, private registrarService: RegistroClienteService) {
    this.formCliente = this.createForm();
  }

  ngOnInit(): void {
   // this.getForm;
  }

  onlyNumberKey(event) {
    return (/^[0-9]*$/i).test(String.fromCharCode(event.keyCode));
  }

  private createForm() {
    return this.formCliente = this.fb.group({
      nomCliente: [null, [Validators.required, Validators.pattern]],
      apeCliente: [null, [Validators.required, Validators.pattern]],
      dniCliente: [null, [Validators.required, Validators.pattern]],
      fonoCliente: [null, [Validators.required, Validators.pattern]],
      emailCliente: [null, [Validators.required, Validators.pattern]],
      passCliente: [null, [Validators.required]]
    });
  }

  public get getForm() {
    return this.formCliente;
  }

  public get fieldNombreCliente() {
    return this.getForm.get('nomCliente');
  }

  public get fieldApellidoCliente() {
    return this.getForm.get('apeCliente');
  }

  public get fieldDniCliente() {
    return this.getForm.get('dniCliente');
  }

  public get fieldFonoCliente() {
    return this.getForm.get('fonoCliente');
  }

  public get fieldCorreoElectronico() {
    return this.getForm.get('emailCliente');
  }

  public get fieldContrasena() {
    return this.getForm.get('passCliente');
  }

  public registrar(){
    // Validaciones
    this.getForm.markAllAsTouched();
    if(this.getForm.invalid) return;

    const cliente: Cliente = {
      nomCliente: this.fieldNombreCliente?.value,
      apeCliente: this.fieldApellidoCliente?.value,
      dniCliente: this.fieldDniCliente?.value,
      fonoCliente: Number(this.fieldFonoCliente?.value),
      emailCliente: this.fieldCorreoElectronico?.value,
      passCliente: this.fieldContrasena?.value
    }
    this.registrarService.registrarCliente(cliente).subscribe(data => this.volver());
  }

  public volver(){
    this.router.navigate([RutasConstante.INGRESAR_SISTEMA]);
  }
}
