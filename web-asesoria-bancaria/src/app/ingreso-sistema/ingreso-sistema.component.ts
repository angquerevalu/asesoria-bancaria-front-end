import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { ClienteResponse } from '../interfaces/cliente.interface';
import { LoginCliente } from '../interfaces/clientelogin.interface';
import { RutasConstante } from '../libs/rutas-constante';
import { IngresoSistemaService } from '../services/ingreso-sistema.service';

@Component({
  selector: 'app-ingreso-sistema',
  templateUrl: './ingreso-sistema.component.html',
  styleUrls: ['./ingreso-sistema.component.css']
})
export class IngresoSistemaComponent implements OnInit {

  public form: FormGroup;
  public loginCliente: ClienteResponse;
  public existeExcepcion = false;

  constructor(private fb: FormBuilder,
    private ingresoService: IngresoSistemaService,
    private router: Router
  ) { this.form = this.createForm(); }

  ngOnInit(): void {
  }
  private createForm() {
    return this.fb.group({
      correoElectronico: [null, [Validators.required]],
      contraseña: [null, [Validators.required]]
    })
  }

  public get getForm() {
    return this.form;
  }

  public get fieldCorreoElectronico() {
    return this.getForm.get('correoElectronico');
  }

  public get fieldContrasena() {
    return this.getForm.get('contraseña');
  }

  public registrar() {
    this.router.navigate([RutasConstante.REGISTRAR_CLIENTE]);
  }

  public ingresar() {
    // Validaciones
    this.getForm.markAllAsTouched()
    if (this.getForm.invalid) return;

    const login: LoginCliente = {
      emailCliente: this.fieldCorreoElectronico?.value,
      passCliente: this.fieldContrasena?.value
    }
    this.ingresoService.ingresarAlSistema(login).subscribe((data) => {
      this.loginCliente = data
      if (this.loginCliente != null) {
        this.existeExcepcion = false;
        var jsonCliente = {"codCliente" : this.loginCliente.codCliente,
                           "nomCliente" : this.loginCliente.nomCliente,
                           "apeCliente" : this.loginCliente.apeCliente,
                           "dniCliente" : this.loginCliente.dniCliente,
                           "fonoCliente" : this.loginCliente.fonoCliente,
                           "emailCliente" : this.loginCliente.emailCliente,
                           "passCliente" : this.loginCliente.passCliente}
        localStorage.setItem("cliente",JSON.stringify(jsonCliente));
        if(this.loginCliente.codCliente == 1){
          this.router.navigate([RutasConstante.MANTENIMIENTO_SISTEMA]);
        }else{
          this.router.navigate([RutasConstante.CLIENTE_CONSULTAS]);
        }

      } else {
        this.existeExcepcion = true;
      }
    });
  }

  public limpiar() {
    this.existeExcepcion = false;
  }

  public salir(){
    this.router.navigate([RutasConstante.PAGES_INFORMATIVAS]);
  }
}
