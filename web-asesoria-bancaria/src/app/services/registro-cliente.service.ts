import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Cliente } from '../interfaces/cliente.interface';

@Injectable({
  providedIn: 'root'
})
export class RegistroClienteService {

  private apiBase: string = environment.urlBase;

    constructor(private http: HttpClient) { }

    registrarCliente(form:Cliente) {
      return this.http.post<Cliente>(this.apiBase + "cliente/registrar",form);
  }
}
