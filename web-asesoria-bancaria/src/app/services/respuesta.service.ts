import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Respuesta } from '../interfaces/respuesta.interface';

@Injectable({
  providedIn: 'root'
})
export class RespuestaService {

  private apiBase: string = environment.urlBase;

  constructor(private http: HttpClient) { }

  registrarRespuesta(form:Respuesta) {
    return this.http.post<Respuesta>(this.apiBase + "respuesta/save",form);
  }

  buscarRespuesta(codConsulta: number) {
    return this.http.get<Respuesta>(this.apiBase + "respuesta/listar/" + codConsulta);
  }
}
