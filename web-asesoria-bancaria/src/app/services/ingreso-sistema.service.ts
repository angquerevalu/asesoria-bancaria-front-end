import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ClienteResponse } from '../interfaces/cliente.interface';
import { LoginCliente } from '../interfaces/clientelogin.interface';

@Injectable({
    providedIn: 'root'
})

export class IngresoSistemaService {

    private apiBase: string = environment.urlBase;

    constructor(private http: HttpClient) { }

    ingresarAlSistema(loginCliente: LoginCliente) {
        return this.http.post<ClienteResponse>(this.apiBase + "cliente/login" , loginCliente);
    }
}
