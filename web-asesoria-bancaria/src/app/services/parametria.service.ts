import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Categoria, Consulta, Estado } from '../interfaces/consulta.interface';

@Injectable({
  providedIn: 'root'
})
export class ParametriaService {

  private apiBase: string = environment.urlBase;

  constructor(private http: HttpClient) { }

  getLstCategoria() : Observable<Categoria[]>{
    return this.http.get<Categoria[]>(this.apiBase + "categoria/listar");
  }

  getLstEstado() : Observable<Estado[]>{
    return this.http.get<Estado[]>(this.apiBase + "estado/listar");
  }

  getConsultas() : Observable<Consulta[]>{
    return this.http.get<Consulta[]>(this.apiBase + "consulta/listar");
  }
}
