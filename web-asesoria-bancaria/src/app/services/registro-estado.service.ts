import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Estado } from '../interfaces/consulta.interface';

@Injectable({
  providedIn: 'root'
})
export class RegistroEstadoService {

  private apiBase: string = environment.urlBase;

  constructor(private http: HttpClient) { }

  registrarEstado(form:Estado) {
    return this.http.post<Estado>(this.apiBase + "estado/save",form);
  }

  eliminarEstado(codEstado : number){
    return this.http.delete(this.apiBase + "estado/delete/" + codEstado);
  }

  modificarEstado(form: Estado) {
    return this.http.put<any>(this.apiBase + "estado/update", form);
  }

  ultimoCodigo() {
    return this.http.get<number>(this.apiBase + "estado/siguienteCodigo");
  }
}
