import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Consulta } from '../interfaces/consulta.interface';

@Injectable({
  providedIn: 'root'
})
export class RegistroConsultaService {

  private apiBase: string = environment.urlBase;

  constructor(private http: HttpClient) { }

  registrarConsulta(form: Consulta) {
    return this.http.post<Consulta>(this.apiBase + "consulta/save", form);
  }

  modificarConsulta(form: Consulta) {
    return this.http.put<any>(this.apiBase + "consulta/update", form);
  }

  listarConsulta(cod_cliente: number) {
    return this.http.get<Consulta[]>(this.apiBase + "consulta/listar/" + cod_cliente);
  }

  eliminarConsulta(codConsulta: number) {
    return this.http.delete(this.apiBase + "consulta/delete/"+ codConsulta);
  }

  ultimoCodigo() {
    return this.http.get<number>(this.apiBase + "consulta/siguienteCodigo");
  }
}
