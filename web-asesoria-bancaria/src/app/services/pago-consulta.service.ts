import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Pago } from '../interfaces/pago.interface';

@Injectable({
  providedIn: 'root'
})
export class PagoConsultaService {

  private apiBase: string = environment.urlBase;

  constructor(private http: HttpClient) { }

  realizarPago(pago: Pago) {
    return this.http.post<Pago>(this.apiBase + "pago/save" , pago);
}
}
