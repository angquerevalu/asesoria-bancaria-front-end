import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Categoria } from '../interfaces/consulta.interface';

@Injectable({
  providedIn: 'root'
})
export class RegistroCategoriaService {

  private apiBase: string = environment.urlBase;

  constructor(private http: HttpClient) { }

  registrarCategoria(form: Categoria) {
    return this.http.post<Categoria>(this.apiBase + "categoria/save", form);
  }

  eliminarCategoria(codCategoria: number) {
    return this.http.delete(this.apiBase + "categoria/delete/" + codCategoria);
  }

  modificarCategoria(form: Categoria) {
    return this.http.put<any>(this.apiBase + "categoria/update", form);
  }

  ultimoCodigo() {
    return this.http.get<number>(this.apiBase + "categoria/siguienteCodigo");
  }
}
