import { Injectable } from '@angular/core';
import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs';
import { ModalConfirmarComponent } from '../components/modal-confirmar/modal-confirmar.component';

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  constructor(config: NgbModalConfig, private md: NgbModal) {
    config.backdrop = 'static';
    config.keyboard = false;
  }

  public showConfirmar(titulo: string, mensaje: string, labelBtnLeft?: string, labelbtnRight?: string): Observable<boolean> {
    const modalRef = this.md.open(ModalConfirmarComponent);
    modalRef.componentInstance.titulo = titulo;
    modalRef.componentInstance.mensaje = mensaje;
    modalRef.componentInstance.labelBtnLeft = labelBtnLeft ?? 'Cancelar';
    modalRef.componentInstance.labelbtnRight = labelbtnRight ?? 'Aceptar';
    return modalRef.componentInstance.respuesta as Observable<boolean>;
  }
}
