import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AgregarConsultaComponent } from './components/agregar-consulta/agregar-consulta.component';
import { IngresoSistemaComponent } from './ingreso-sistema/ingreso-sistema.component';
import { IngresoSistemaService } from './services/ingreso-sistema.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http'
import { DatePipe } from '@angular/common';
import { RegistrarClienteComponent } from './registrar-cliente/registrar-cliente.component';
import { RegistroClienteService } from './services/registro-cliente.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ClienteConsultasComponent } from './cliente-consultas/cliente-consultas.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { AgregarPagoComponent } from './components/agregar-pago/agregar-pago.component';
import { DataTablesModule } from 'angular-datatables';
import { ParametriaService } from './services/parametria.service';
import { MantenimientoSistemaComponent } from './mantenimiento-sistema/mantenimiento-sistema.component';
import { AgregarEstadoComponent } from './mantenimiento-sistema/agregar-estado/agregar-estado.component';
import { AgregarCategoriaComponent } from './mantenimiento-sistema/agregar-categoria/agregar-categoria.component';
import { RegistroEstadoService } from './services/registro-estado.service';
import { RegistroCategoriaService } from './services/registro-categoria.service';
import { PagoConsultaService } from './services/pago-consulta.service';
import { ResponderConsultaComponent } from './mantenimiento-sistema/responder-consulta/responder-consulta.component';
import { RespuestaService } from './services/respuesta.service';
import { ModalConfirmarComponent } from './components/modal-confirmar/modal-confirmar.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { ModalService } from './services/modal.service';
import { SpinnerService } from './services/spinner.service';
import { InterceptorService } from './services/interceptor.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MostrarConsultaComponent } from './components/mostrar-consulta/mostrar-consulta.component';
import { PagesInformativasComponent } from './pages-informativas/pages-informativas.component';
import { TarjetaCreditoComponent } from './pages-informativas/tarjeta-credito/tarjeta-credito.component';
import { PresupuestoComponent } from './pages-informativas/presupuesto/presupuesto.component';
import { SeguridadFinancieraComponent } from './pages-informativas/seguridad-financiera/seguridad-financiera.component';
import { CargosComunesFrecuentesComponent } from './pages-informativas/cargos-comunes-frecuentes/cargos-comunes-frecuentes.component';
import { CuentaBancariaComponent } from './pages-informativas/cuenta-bancaria/cuenta-bancaria.component';
import { OperacionesActivasPasivasComponent } from './pages-informativas/operaciones-activas-pasivas/operaciones-activas-pasivas.component';

@NgModule({
  declarations: [
    AppComponent,
    AgregarConsultaComponent,
    IngresoSistemaComponent,
    RegistrarClienteComponent,
    ClienteConsultasComponent,
    AgregarPagoComponent,
    MantenimientoSistemaComponent,
    AgregarEstadoComponent,
    AgregarCategoriaComponent,
    ResponderConsultaComponent,
    ModalConfirmarComponent,
    MostrarConsultaComponent,
    PagesInformativasComponent,
    TarjetaCreditoComponent,
    PresupuestoComponent,
    SeguridadFinancieraComponent,
    CargosComunesFrecuentesComponent,
    CuentaBancariaComponent,
    OperacionesActivasPasivasComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    NgbModule,
    NgSelectModule,
    DataTablesModule,
    NgxSpinnerModule,
    BrowserAnimationsModule
  ],
  providers: [IngresoSistemaService, RegistroClienteService, ParametriaService, DatePipe, RegistroEstadoService, RegistroCategoriaService, PagoConsultaService, RespuestaService, ModalService, SpinnerService, 
    { provide: HTTP_INTERCEPTORS, useClass: InterceptorService, multi:true}],
  bootstrap: [AppComponent],
  schemas:[
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class AppModule { }
