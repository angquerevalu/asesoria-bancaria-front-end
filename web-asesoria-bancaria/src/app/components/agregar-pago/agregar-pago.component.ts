import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Pago } from 'src/app/interfaces/pago.interface';
import { PagoConsultaService } from 'src/app/services/pago-consulta.service';

@Component({
  selector: 'app-agregar-pago',
  templateUrl: './agregar-pago.component.html',
  styleUrls: ['./agregar-pago.component.css']
})
export class AgregarPagoComponent implements OnInit {

  public form: FormGroup;
  @Output() lstData = new EventEmitter<any>()

  constructor(private fb: FormBuilder,
    private activeModal: NgbActiveModal,
    private pagoService: PagoConsultaService) { this.form = this.createForm(); }

  ngOnInit(): void {

  }

  private createForm() {
    return this.fb.group({
      numTarjeta: [null, [Validators.required], Validators.pattern],
      monthExp: [null, [Validators.required, Validators.pattern]],
      yearExp: [null, [Validators.required], Validators.min(2021)],
      ccv: [null, [Validators.required, Validators.pattern]],
      nomTitular: [null, [Validators.required, Validators.pattern]]
    })
  }

  public get getForm() {
    return this.form;
  }

  public get fieldNumTarjeta() {
    return this.getForm.get('numTarjeta');
  }

  public get fieldMonthExp() {
    return this.getForm.get('monthExp');
  }

  public get fieldYearExp() {
    return this.getForm.get('yearExp');
  }

  public get fieldCcv() {
    return this.getForm.get('ccv');
  }

  public get fieldNomTitular() {
    return this.getForm.get('nomTitular');
  }
  public cerrar(){
    this.activeModal.close();
  }

  public pagar(){
    this.getForm.markAllAsTouched();
    if (this.getForm.invalid) return;
    const pago: Pago = {
      monthExp: this.fieldMonthExp.value,
      yearExp: this.fieldYearExp.value,
      ccv: this.fieldCcv.value,
      nomTitular: this.fieldNomTitular.value,
      cliente: JSON.parse(localStorage.getItem("cliente"))
    }
    this.pagoService.realizarPago(pago).subscribe((data) => {
      this.lstData.emit(data);
      this.activeModal.close();
    });
  }

  onlyNumberKey(event) {
    return (/^[0-9]*$/i).test(String.fromCharCode(event.keyCode));
  }
}
