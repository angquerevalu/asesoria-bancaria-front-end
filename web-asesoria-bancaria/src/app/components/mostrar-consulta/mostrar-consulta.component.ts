import { DatePipe } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Consulta } from 'src/app/interfaces/consulta.interface';
import { Respuesta } from 'src/app/interfaces/respuesta.interface';
import { RespuestaService } from 'src/app/services/respuesta.service';

@Component({
  selector: 'app-mostrar-consulta',
  templateUrl: './mostrar-consulta.component.html',
  styleUrls: ['./mostrar-consulta.component.css']
})
export class MostrarConsultaComponent implements OnInit {

  public form: FormGroup;

  @Input() consulta: Consulta;
  public existeRespuesta = false;

  constructor(private fb: FormBuilder,
    public datePipe: DatePipe,
    private activeModal: NgbActiveModal,
    private respuestaService: RespuestaService) { this.form = this.createForm(); }

  ngOnInit(): void {
    this.setValues(this.consulta);
    this.respuestaService.buscarRespuesta(this.consulta.codConsulta).subscribe((data) => {
      if (data != null) {
        this.existeRespuesta = true;
        this.setValues(this.consulta, data);
      }else{
        this.existeRespuesta = false;
      }
    })

    
  }

  private createForm() {
    return this.fb.group({
      desConsulta: [],
      fechaConsulta: [],
      desRespuesta: [],
      fechaRespuesta: []
    })
  }

  public get getForm() {
    return this.form;
  }

  public get fieldDesConsulta() {
    return this.getForm.get('desConsulta');
  }

  public get fieldFechaConsulta() {
    return this.getForm.get('fechaConsulta');
  }

  public get fieldDesRespuesta() {
    return this.getForm.get('desRespuesta');
  }

  public get fieldFechaRespuesta() {
    return this.getForm.get('fechaRespuesta');
  }

  public cerrar() {
    this.activeModal.close();
  }

  private setValues(consulta: Consulta, respuesta?: Respuesta): void {
    this.fieldDesConsulta.setValue(consulta.desConsulta);
    this.fieldFechaConsulta.setValue(this.fechaConsulta(consulta.fechaConsulta));
    if (this.existeRespuesta) {
      this.fieldDesRespuesta.setValue(respuesta.desRespuesta);
      this.fieldFechaRespuesta.setValue(respuesta.fechaRespuesta);
    }
  }

  public fechaConsulta(fecha: string) {
    const fechaActual = String(fecha);
    const fechaConsulta = this.datePipe.transform(fechaActual, "yyyy-MM-dd");

    return fechaConsulta;
  }

}
