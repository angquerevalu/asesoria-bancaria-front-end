import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-modal-confirmar',
  templateUrl: './modal-confirmar.component.html',
  styleUrls: ['./modal-confirmar.component.css']
})
export class ModalConfirmarComponent implements OnInit {

  @Input() titulo : string;
  @Input() mensaje : string;
  @Input() labelBtnLeft : string;
  @Input() labelbtnRight : string;
  @Output() respuesta = new EventEmitter<boolean>();

  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit(): void {
  }

  public cancelar(): void {
    this.respuesta.emit(false);
    this.activeModal.dismiss("Cross click");
  }

  public aceptar(): void {
    this.respuesta.emit(true);
    this.activeModal.dismiss("Cross click");
  }
}
