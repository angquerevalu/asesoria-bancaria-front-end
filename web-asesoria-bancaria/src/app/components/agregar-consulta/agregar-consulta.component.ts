import { DatePipe } from '@angular/common';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Categoria, Consulta, Estado } from 'src/app/interfaces/consulta.interface';
import { ParametriaService } from 'src/app/services/parametria.service';
import { RegistroConsultaService } from 'src/app/services/registro-consulta.service';
import { AgregarPagoComponent } from '../agregar-pago/agregar-pago.component';

@Component({
  selector: 'app-agregar-consulta',
  templateUrl: './agregar-consulta.component.html',
  styleUrls: ['./agregar-consulta.component.css']
})
export class AgregarConsultaComponent implements OnInit {

  public form: FormGroup;
  public mensaje!: "error";
  public lstCategoria = [];
  public desEstado: string;
  public pagoRealizado = false;

  @Output() lstRegistroActualizado = new EventEmitter<any>();

  @Input() registro: Consulta;
  @Input() inputIndex: number;
  @Input() lstRegistro: Consulta[];

  public ultimoCodigo : number;

  constructor(private fb: FormBuilder,
    private activeModal: NgbActiveModal,
    private modalService: NgbModal,
    private parametriaService: ParametriaService,
    public datePipe: DatePipe,
    private consultaService: RegistroConsultaService
  ) {
    this.form = this.createForm();
  }

  ngOnInit(): void {
    this.parametriaService.getLstCategoria().subscribe((data) => {
      this.lstCategoria = data;
    });
    this.parametriaService.getLstEstado().subscribe((data) => {
      for (var key in data) {
        if (data[key].codEstado == 1) {
          this.desEstado = data[key].desEstado;
        }
      }
    });
    this.fieldFechaConsulta.setValue(this.fechaConsulta());

    if (this.registro) {
      this.setValuesEdit(this.registro);
      this.pagoRealizado = true;
    }
  }

  ngDoCheck(): void {
    this.montoAPagar();
  }

  private createForm() {
    return this.fb.group({
      desConsulta: [null, [Validators.required]],
      fechaConsulta: [null, []],
      categoria: [null, [Validators.required]],
      precioCategoria: [null, [Validators.required]]
    })
  }

  public get getForm() {
    return this.form;
  }

  public get fieldDesConsulta() {
    return this.getForm.get('desConsulta');
  }

  public get fieldFechaConsulta() {
    return this.getForm.get('fechaConsulta');
  }

  public get fieldCategoria() {
    return this.getForm.get('categoria');
  }

  public get fieldPrecioCategoria() {
    return this.getForm.get('precioCategoria');
  }

  public cerrar() {
    this.activeModal.close();
  }

  public getDesNoValido(): boolean {
    return this.fieldDesConsulta.invalid && this.fieldDesConsulta.touched;
  }

  public resetValidatorsForm(): void {
    Object.values(this.getForm.controls).forEach(control => {
      control.setValue(null);
    });
    this.getForm.markAllAsTouched();
  }

  public agregar() {
    const estado: Estado = {
      codEstado: 1,
      desEstado: this.desEstado
    }

    const categoria: Categoria = {
      codCategoria: this.fieldCategoria.value,
      desCategoria: this.obtenerDescripcionCategoria(this.fieldCategoria.value),
      precioCategoria: this.obtenerMontoAPagar(this.fieldCategoria.value),
    }

    this.consultaService.ultimoCodigo().subscribe((data) => {
       this.ultimoCodigo = data;
    })
    const registro: Consulta = {
      codConsulta: this.ultimoCodigo,
      desConsulta: this.fieldDesConsulta?.value,
      fechaConsulta: this.fieldFechaConsulta?.value,
      estado: estado,
      cliente: JSON.parse(localStorage.getItem("cliente")),
      categoria: categoria
    }

    if (!this.registro) {
      this.consultaService.registrarConsulta(registro).subscribe((data) => {
        if (data != null) {
          this.lstRegistroActualizado.emit(registro);
        }
      })
    }else {
      console.log(this.lstRegistro);
      const registroActualizado: Consulta = {
        codConsulta: this.registro.codConsulta,
        desConsulta: this.fieldDesConsulta?.value,
        fechaConsulta: this.fieldFechaConsulta?.value,
        estado: estado,
        cliente: JSON.parse(localStorage.getItem("cliente")),
        categoria: categoria
      }
      if (!this.equals(this.registro, registroActualizado)) {
        this.lstRegistro[this.inputIndex] = registroActualizado;
        this.consultaService.modificarConsulta(registroActualizado).subscribe();
      }
    }

    this.activeModal.close();
  }

  public pagar() {
    const modal = this.modalService.open(AgregarPagoComponent, {
      backdrop: "static",
      keyboard: false,
      size: 'lg'
    });

    modal.componentInstance.lstData.subscribe((data) => {
      if (data != null) {
        this.pagoRealizado = true;
      }
    })

  }

  public montoAPagar() {
    if (this.fieldCategoria?.value != null) {
      this.fieldPrecioCategoria?.setValue("S/ " + this.obtenerMontoAPagar(this.fieldCategoria.value));
    }
  }
  public fechaConsulta() {
    const fechaActual = String(new Date());
    const fechaConsulta = this.datePipe.transform(fechaActual, "yyyy-MM-dd");

    return fechaConsulta;
  }

  public obtenerDescripcionCategoria(val: string): string {
    const desc = this.lstCategoria.filter((x) => x.codCategoria === val);
    return desc.length !== 0 ? desc[0].desCategoria : "";
  }

  public obtenerMontoAPagar(val: string): string {
    const precio = this.lstCategoria.filter((x) => x.codCategoria === val);
    return precio.length !== 0 ? precio[0].precioCategoria : "";
  }

  public pagoisChecked(event) {
    if (this.pagoRealizado) {
      this.pagoRealizado = event.currentTarget.checked;
    }
  }

  public disabledButton(): boolean {
    return this.pagoRealizado == true ? false : true;
  }

  public disabledPagarButton(): boolean {
    return this.pagoRealizado == true ? true : false;
  }

  private setValuesEdit(consulta: Consulta): void {
    this.fieldDesConsulta.setValue(consulta.desConsulta);
    this.fieldFechaConsulta.setValue(this.fechaConsulta());
    this.fieldCategoria.setValue(consulta.categoria.codCategoria);
    this.fieldPrecioCategoria.setValue(consulta.categoria.precioCategoria)
  }

  private equals(obj: Consulta, objNuevo: Consulta): boolean {
    return (objNuevo.desConsulta === obj.desConsulta &&
      objNuevo.fechaConsulta === obj.fechaConsulta &&
      objNuevo.categoria === obj.categoria &&
      objNuevo.categoria.precioCategoria === obj.categoria.precioCategoria)
  }
}
