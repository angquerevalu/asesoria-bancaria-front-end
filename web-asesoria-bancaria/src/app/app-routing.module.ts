import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ClienteConsultasComponent } from './cliente-consultas/cliente-consultas.component';
import { IngresoSistemaComponent } from './ingreso-sistema/ingreso-sistema.component';
import { RutasConstante } from './libs/rutas-constante';
import { MantenimientoSistemaComponent } from './mantenimiento-sistema/mantenimiento-sistema.component';
import { CargosComunesFrecuentesComponent } from './pages-informativas/cargos-comunes-frecuentes/cargos-comunes-frecuentes.component';
import { CuentaBancariaComponent } from './pages-informativas/cuenta-bancaria/cuenta-bancaria.component';
import { OperacionesActivasPasivasComponent } from './pages-informativas/operaciones-activas-pasivas/operaciones-activas-pasivas.component';
import { PagesInformativasComponent } from './pages-informativas/pages-informativas.component';
import { PresupuestoComponent } from './pages-informativas/presupuesto/presupuesto.component';
import { SeguridadFinancieraComponent } from './pages-informativas/seguridad-financiera/seguridad-financiera.component';
import { TarjetaCreditoComponent } from './pages-informativas/tarjeta-credito/tarjeta-credito.component';
import { RegistrarClienteComponent } from './registrar-cliente/registrar-cliente.component';

const routes: Routes = [
  {path: RutasConstante.INGRESAR_SISTEMA, component: IngresoSistemaComponent},
  {path: RutasConstante.REGISTRAR_CLIENTE, component: RegistrarClienteComponent},
  {path: RutasConstante.CLIENTE_CONSULTAS, component: ClienteConsultasComponent},
  {path: RutasConstante.MANTENIMIENTO_SISTEMA, component: MantenimientoSistemaComponent},
  {path: RutasConstante.PAGES_INFORMATIVAS, component: PagesInformativasComponent},
  {path: RutasConstante.USO_TARJETAS, component: TarjetaCreditoComponent},
  {path: RutasConstante.PRESUPUESTO, component: PresupuestoComponent},
  {path: RutasConstante.SEGURIDAD_FINANCIERA, component: SeguridadFinancieraComponent},
  {path: RutasConstante.CARGOS_COMUNES, component: CargosComunesFrecuentesComponent},
  {path: RutasConstante.CUENTA_BANCARIA, component: CuentaBancariaComponent},
  {path: RutasConstante.OPERACIONES_ACTIVAS_PASIVAS, component: OperacionesActivasPasivasComponent},
  {path: "", pathMatch: "full", redirectTo: RutasConstante.PAGES_INFORMATIVAS}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
