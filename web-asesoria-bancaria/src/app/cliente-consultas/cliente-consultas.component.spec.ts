import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClienteConsultasComponent } from './cliente-consultas.component';

describe('ClienteConsultasComponent', () => {
  let component: ClienteConsultasComponent;
  let fixture: ComponentFixture<ClienteConsultasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ClienteConsultasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ClienteConsultasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
