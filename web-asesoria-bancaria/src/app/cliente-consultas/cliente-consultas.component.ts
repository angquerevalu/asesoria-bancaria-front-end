import { DatePipe } from '@angular/common';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { AgregarConsultaComponent } from '../components/agregar-consulta/agregar-consulta.component'
import { MostrarConsultaComponent } from '../components/mostrar-consulta/mostrar-consulta.component';
import { Consulta } from '../interfaces/consulta.interface';
import { dtOptions } from '../libs/config';
import { RenderizarPaginacion } from '../libs/renderizar-paginacion';
import { RutasConstante } from '../libs/rutas-constante';
import { ModalService } from '../services/modal.service';
import { RegistroConsultaService } from '../services/registro-consulta.service';

@Component({
  selector: 'app-cliente-consultas',
  templateUrl: './cliente-consultas.component.html',
  styleUrls: ['./cliente-consultas.component.css']
})
export class ClienteConsultasComponent extends RenderizarPaginacion implements OnInit {

  @Input() lstRegistro = [];

  public dtOptions: DataTables.Settings = dtOptions;
  public dtTrigger: Subject<any> = new Subject();
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

  public codCliente: number;
  public codConsulta: number;
  public json : any;

  constructor(private modalService: NgbModal,
    public datePipe: DatePipe,
    private router: Router,
    private consultaService: RegistroConsultaService
  ) { super(); }

  ngOnInit(): void {
    this.json = JSON.parse(localStorage.getItem("cliente"));
    if(this.json == null){
      this.router.navigate([RutasConstante.INGRESAR_SISTEMA]);
    }else {
    for (var key in this.json) {
      if (key == "codCliente") {
        this.codCliente = this.json[key];
      }
    }

    this.consultaService.listarConsulta(this.codCliente).subscribe((data) => {
      if (data != null) {
        this.lstRegistro = data;
        this.rerender();
      }
    })
  }
  }

  ngAfterViewInit(): void { this.dtTrigger.next(); }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }

  public agregar() {
    const modal = this.modalService.open(AgregarConsultaComponent, {
      backdrop: "static",
      keyboard: false,
      size: 'lg'
    });

    modal.componentInstance.lstRegistroActualizado.subscribe((data) => {
      this.lstRegistro.push(data);
      this.rerender();
      this.ngOnInit();
    })
  }

  public editar(item: Consulta, index: number) {
    const modal = this.modalService.open(AgregarConsultaComponent, {
      backdrop: "static",
      keyboard: false,
      size: 'lg'
    });
    modal.componentInstance.registro = item;
    modal.componentInstance.inputIndex = index;
    modal.componentInstance.lstRegistro = this.lstRegistro;
  }

  public fechaConsulta(fecha: string) {
    const fechaActual = String(fecha);
    const fechaConsulta = this.datePipe.transform(fechaActual, "yyyy-MM-dd");

    return fechaConsulta;
  }

  public verConsulta(item: Consulta) {
    const modal = this.modalService.open(MostrarConsultaComponent, {
      backdrop: "static",
      keyboard: false,
      size: 'lg'
    });

    modal.componentInstance.consulta = item;
  }

  public salir(){
    this.router.navigate([RutasConstante.INGRESAR_SISTEMA]);
    localStorage.setItem("cliente", null);
  }

}
