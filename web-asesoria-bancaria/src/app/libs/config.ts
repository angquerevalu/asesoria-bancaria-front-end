export const dtOptions = {
    responsive: true,
    retrieve: true,
    paging: false,
    ordering: false,
    info: false,
    searching: false,
    language: {
        zeroRecords: "No hay registros disponibles"
    }
}