export class RutasConstante {
    public static readonly REGISTRAR_CLIENTE = "registrar-cliente";
    public static readonly INGRESAR_SISTEMA = "ingreso-sistema";
    public static readonly CLIENTE_CONSULTAS = "cliente-consultas";
    public static readonly MANTENIMIENTO_SISTEMA = "mantenimiento-sistema";
    public static readonly PAGES_INFORMATIVAS = "pages-informativas";
    public static readonly USO_TARJETAS = "tarjeta-credito";
    public static readonly PRESUPUESTO = "presupuesto";
    public static readonly SEGURIDAD_FINANCIERA = "seguridad-financiera";
    public static readonly CARGOS_COMUNES = "cargos-comunes-frecuentes";
    public static readonly CUENTA_BANCARIA = "cuenta-bancaria";
    public static readonly OPERACIONES_ACTIVAS_PASIVAS = "operaciones-activas-pasivas";
}